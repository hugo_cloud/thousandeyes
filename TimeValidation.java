import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeValidation {
	
	Map<String,List<String>> dependencyMap = new HashMap<String,List<String>>();
	Map<String,Integer> unitTimeMap = new HashMap<String,Integer>();
	Map<String,Integer> tempUnitTimeMap = new HashMap<String,Integer>();
	
	boolean isValidated = false;
	
	Map<String,Integer> determineTimeTask(List<Task> taskList){
		
		for(Task t : taskList) {
			if(t.getIds().size() < 1) {
				System.out.print("Task with no dependency - ");
				System.out.println(t.getId());
				unitTimeMap.put(t.getId(), t.getUnitTime());
			}else {
				System.out.print("Task with dependency - ");
				System.out.println(t.getId());
				dependencyMap.put(t.getId(), t.getIds());
				unitTimeMap.put(t.getId(), null);
				tempUnitTimeMap.put(t.getId(), t.getUnitTime());
			}
		}
		
		while(!isValidated) {
			isValidated=true;
			for(Map.Entry<String, List<String>> entry : dependencyMap.entrySet()) {		
				
				if(unitTimeMap.get(entry.getKey()) == null) {
					System.out.println("Validating task (" + entry.getKey() + ") with dependencies " );
					isValidated=false;
					validateDependency(entry.getKey());
				}else {
					System.out.println("Task (" + entry.getKey() +") has been validated" );
					break;
				}				
				
			}
		}
		
		
		return unitTimeMap;
		
	}
	
	void validateDependency(String id) {
		System.out.println("Validating unit time dependencies for task - " + id);
		boolean isUnitTimeNull = false;
		
		List<String> ids = dependencyMap.get(id);
		int tempValue = 0;
		for(String i : ids) {
			Integer unitTime = unitTimeMap.get(i);
			if(unitTime != null) {
				System.out.println("there is a unit time for id - " + i);
				if(tempValue < unitTime) {
					tempValue = unitTime;
				}
			}else {
				System.out.println("there is not a unit time for id - " + i);
				isUnitTimeNull = true;
				validateDependency(i);
			}
		}
		if(!isUnitTimeNull) {
			int tempUnitTime = tempUnitTimeMap.get(id);
			System.out.println("setting unit time for task - " + id);
			unitTimeMap.put(id, tempValue + tempUnitTime);
		}			
		
		
	}
	
	public static void main(String[] args) {
		Task task1 = new Task();
		Task task2 = new Task();
		Task task3 = new Task();
		Task task4 = new Task();
		Task task5 = new Task();
		Task task6 = new Task();
		List<Task> taskList= new ArrayList<Task>();
		
		task1.setId("A");
		task1.setUnitTime(5);
		taskList.add(task1);
		task2.setId("B");
		task2.setUnitTime(7);
		taskList.add(task2);
		task3.setId("C");		
		List<String> ids = new ArrayList<String>();
		ids.add("B");
		ids.add("D");
		ids.add("E");
		task3.setIds(ids);
		task3.setUnitTime(4);
		taskList.add(task3);
		task4.setId("D");
		task4.setUnitTime(15);
		taskList.add(task4);
		task5.setId("E");
		List<String> ids1 = new ArrayList<String>();
		ids1.add("A");
		ids1.add("B");
		task5.setIds(ids1);
		task5.setUnitTime(2);
		taskList.add(task5);
		task6.setId("F");
		task6.setUnitTime(3);
		taskList.add(task6);
		
				
		Map<String,Integer> taskTime = new TimeValidation().determineTimeTask(taskList);
		
		System.out.println("");
		System.out.println("Final estimation of time for each task : ");
		
		for(Map.Entry<String, Integer> entry : taskTime.entrySet()) {			
			System.out.print("Task (" + entry.getKey() +") most be completed in (" );
			System.out.println(entry.getValue() + ") unit of times");
		}
	}

}
