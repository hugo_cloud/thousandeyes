import java.util.List;
import java.util.ArrayList;


public class Task {
	
	private String id;
	List<String> ids = new ArrayList<String>();
	private int unitTime;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<String> getIds() {
		return ids;
	}
	public void setIds(List<String> ids) {
		this.ids = ids;
	}
	public int getUnitTime() {
		return unitTime;
	}
	public void setUnitTime(int unitTime) {
		this.unitTime = unitTime;
	}
	
	

}
